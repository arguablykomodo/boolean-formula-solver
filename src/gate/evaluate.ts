import { GateType, Gate } from "./gate";

const algorithms: { [x: number]: (inputs: boolean[]) => boolean } = {
  [GateType.Not]: i => !i[0],
  [GateType.And]: i => i.reduce((p, c) => p && c),
  [GateType.Or]: i => i.reduce((p, c) => p || c),
  [GateType.Xor]: i => i.reduce((p, c) => p !== c)
};

function evaluate(gate: Gate, variables: { [name: string]: boolean }): boolean {
  if (gate.type === GateType.Variable) return variables[gate.var!];
  return algorithms[gate.type](gate.children.map(g => evaluate(g, variables)));
}

export default evaluate;
