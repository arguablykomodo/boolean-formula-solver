enum GateType {
  Not,
  And,
  Or,
  Xor,
  Variable
}

interface Gate {
  type: GateType;
  children?: Gate[];
  var?: string;
}

const operators: { [x: string]: GateType } = {
  ".": GateType.And,
  "+": GateType.Or,
  "°": GateType.Xor
};

const functions: { [x: string]: GateType } = {
  "!": GateType.Not
};

const gates = {
  ...operators,
  ...functions
};

export { Gate, GateType, operators, functions, gates };
