import { Gate, GateType, operators, functions, gates } from "./gate";

const peek = <T>(array: T[]): T => array[array.length - 1];

function isLetter(char: string) {
  const code = char.charCodeAt(0);
  return (code >= 65 && code <= 90) || (code >= 97 && code <= 122);
}

function moveGate(outStack: Gate[], symbol: string) {
  const children: Gate[] = [];
  if (symbol in operators && outStack.length >= 2) {
    const child = outStack.pop();
    children.push(
      outStack.pop(),
      ...(child.type === operators[symbol] ? child.children : [child])
    );
  } //
  else if (symbol in functions && outStack.length >= 1)
    children.push(outStack.pop());
  else {
    if (symbol === undefined || symbol === "(")
      throw new Error("Mismatched parenthesis");
    throw new Error("Invalid Formula");
  }
  outStack.push({
    type: gates[symbol],
    children
  });
}

// For more info:
// https://en.wikipedia.org/wiki/Shunting-yard_algorithm#The_algorithm_in_detail
function step(stack: string[], output: Gate[], vars: string[], char: string) {
  if (isLetter(char)) {
    const lower = char.toLowerCase();
    if (vars.indexOf(lower) === -1) vars.push(lower);
    output.push({ type: GateType.Variable, var: lower });
  } //
  else if (char in functions) stack.push(char);
  else if (char in operators) {
    let opPeek = peek(stack);
    while (
      (opPeek in functions || operators[opPeek] < operators[char]) &&
      opPeek !== "("
    ) {
      moveGate(output, stack.pop());
      opPeek = peek(stack);
    }
    stack.push(char);
  } //
  else if (char === "(") stack.push(char);
  else if (char === ")") {
    while (peek(stack) !== "(") moveGate(output, stack.pop());
    stack.pop();
  }
}

function isImplicit(last: string, curr: string) {
  const lastLetter = isLetter(last);
  const currLetter = isLetter(curr);
  return (
    (lastLetter && currLetter) ||
    (last === ")" && currLetter) ||
    (lastLetter && curr === "(") ||
    (last === ")" && curr === "(") ||
    (lastLetter && curr in functions)
  );
}

function compile(input: string) {
  const vars: string[] = [];
  const stack: string[] = [];
  const output: Gate[] = [];

  if (input.length === 0)
    throw new Error("Umm, you should probably write a formula?");

  let fnCounter = 0;
  let lastChar = "";
  for (const char of input) {
    if (!(char in gates) && !isLetter(char) && char !== "(" && char !== ")")
      throw new Error("Invalid Character");
    if (!isLetter(char))
      while (fnCounter > 0) {
        step(stack, output, vars, ")");
        fnCounter--;
      }
    if (isImplicit(lastChar, char)) step(stack, output, vars, ".");
    step(stack, output, vars, char);
    if (char in functions) {
      step(stack, output, vars, "(");
      fnCounter++;
    }
    lastChar = char;
  }

  while (fnCounter > 0) {
    step(stack, output, vars, ")");
    fnCounter--;
  }

  while (stack.length !== 0) moveGate(output, stack.pop());
  if (output.length > 1) throw new Error("Something went horribly wrong...");
  vars.sort();
  return { vars, formula: output[0] };
}

export default compile;
