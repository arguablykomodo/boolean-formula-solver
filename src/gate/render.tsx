import { h } from "preact";
import { Gate, GateType } from "./gate";

const gates: { [x: number]: (x: number, y: number, r: number) => string } = {
  [GateType.And]: (x, y, r) => `M ${x} ${y} a ${r} ${r} 0 0 1 0 ${r * 2}`,
  [GateType.Or]: (x, y, r) =>
    gates[GateType.And](x, y, r) + `a ${r / 2} ${r} 0 0 0 0 ${-r * 2}`,
  [GateType.Xor]: (x, y, r) => gates[GateType.Or](x, y, r)
};

const functions: { [x: number]: (x: number, y: number) => string } = {
  [GateType.Not]: (x, y) => `M ${x} ${y} l -10 -5 v 10 z`
};

const paths: { [x: number]: (i: number, r: number) => number } = {
  [GateType.Not]: () => 10,
  [GateType.And]: () => 10,
  [GateType.Or]: (i, r) =>
    10 + (r / 2 / r) * Math.sqrt(r ** 2 - (i * 20 + 10 - r) ** 2),
  [GateType.Xor]: (i, r) => paths[GateType.Or](i, r)
};

const extras: { [x: number]: (x: number, y: number, r: number) => string } = {
  [GateType.Xor]: (x, y, r) =>
    `M ${x - 7} ${y} a ${r / 2} ${r} 0 0 1 0 ${r * 2}`
};

interface RenderOutput {
  w: number;
  h: number;

  gates: string;
  paths: string;
  texts: JSX.Element[];
}

const M = 3;

function render(gate: Gate, x: number, y: number): RenderOutput {
  if (gate.type === GateType.Variable)
    return {
      w: 10,
      h: 20,
      gates: "",
      paths: "",
      texts: [
        <text x={x + 10} y={y + 10}>
          {gate.var}
        </text>
      ]
    };

  if (gate.type in functions) {
    const result = render(gate.children[0], x, y);
    const gateX = x + result.w + 20;
    const gateY = y + result.h / 2;
    result.gates += functions[gate.type](gateX, gateY);
    result.paths += `M ${gateX - 10} ${gateY} h -10`;
    result.w += 20;
    return result;
  }

  const r = gate.children.length * 10;
  const widths = [];
  const heights = [];
  const result: RenderOutput = { w: 0, h: 0, gates: "", paths: "", texts: [] };
  for (const child of gate.children) {
    const { w, h, gates, paths, texts } = render(child, x, y + result.h);
    widths.push(w);
    heights.push(h + M);
    result.gates += gates;
    result.paths += paths;
    result.texts.push(...texts);
    result.w = Math.max(result.w, w);
    result.h += h + M;
  }
  const middle = y + result.h / 2;

  let height = 0;
  for (let i = 0; i < gate.children.length; i++) {
    const inX = widths[i];
    const inY = y + height + heights[i] / 2;
    const outX = result.w + 20;
    const outY = middle - r + (i / gate.children.length) * r * 2 + 10;
    const extraX = paths[gate.type](i, r);
    result.paths += `M ${inX} ${inY} H ${outX - 10} L ${outX} ${outY}`;
    result.paths += `h ${extraX}`;
    height += heights[i];
  }

  result.gates += gates[gate.type](x + result.w + 30, middle - r, r) + "z";
  if (gate.type in extras)
    result.paths += extras[gate.type](x + result.w + 30, middle - r, r);
  result.w += r + 30;
  return result;
}

export default render;
