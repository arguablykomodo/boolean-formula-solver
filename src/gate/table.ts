import { Gate, GateType } from "./gate";
import evaluate from "./evaluate";

type Table = boolean[];

const getBit = (bit: number, length: number, variable: number) =>
  ((bit >> (length - 1 - variable)) & 1) === 1;

function fromFormula(vars: string[], formula: Gate) {
  const table: Table = [];
  const context: { [x: string]: boolean } = {};
  for (let i = 0; i < 2 ** vars.length; i++) {
    vars.forEach((v, j, { length: l }) => (context[v] = getBit(i, l, j)));
    table.push(evaluate(formula, context));
  }
  return table;
}

function toFormula(table: Table, vars: string[]) {
  const count1s = table.reduce((p, c) => p + (c ? 1 : 0), 0);
  if (count1s === 0 || count1s === table.length)
    throw new Error("There should be at least one 1 or 0");
  const more1s = count1s > table.length / 2;
  const gate: Gate = {
    type: more1s ? GateType.And : GateType.Or,
    children: []
  };
  table.forEach((bit, i) => {
    if (bit !== more1s)
      gate.children!.push({
        type: more1s ? GateType.Or : GateType.And,
        children: vars.map<Gate>((v, j, { length: l }) => {
          const variable = { type: GateType.Variable, var: v };
          if (getBit(i, l, j) !== more1s) return variable;
          else return { type: GateType.Not, children: [variable] };
        })
      });
  });
  return gate.children!.length === 1 ? gate.children![0] : gate;
}

export { Table, fromFormula, toFormula };
