import { Gate, gates, GateType, functions, operators } from "./gate";

const symbols = (() => {
  const out: { [x: number]: string } = {};
  for (const gate in gates) {
    out[gates[gate]] = gate;
  }
  return out;
})();

const isIn = (thing: GateType, place: { [x: string]: GateType }) =>
  symbols[thing] in place;

function toString(gate: Gate, parent: GateType = 5) {
  if (gate === undefined) return "";
  let out = "";

  if (gate.type === GateType.Variable) return gate.var!;
  if (isIn(gate.type, functions))
    out = symbols[gate.type] + toString(gate.children[0], gate.type);

  let isImplicit = true;
  if (isIn(gate.type, operators))
    out = gate.children
      .map(g => {
        if (g.type !== GateType.Variable) isImplicit = false;
        return toString(g, gate.type);
      })
      .join(gate.type === GateType.And && isImplicit ? "" : symbols[gate.type]);

  if (gate.type > parent) out = `(${out})`;
  return out;
}

export default toString;
