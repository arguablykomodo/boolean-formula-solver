import "./output.scss";
import { h, Component } from "preact";
import { GateType, Gate } from "../gate/gate";

const operators = {
  [GateType.And]: "·",
  [GateType.Or]: "+",
  [GateType.Xor]: "⊕"
};

const functions = {
  [GateType.Not]: (formula: Gate) => {
    return <span className="not">{render(formula)}</span>;
  }
};

function render(formula: Gate) {
  if (formula.type in operators)
    return formula.children.map((c, i, { length: l }) => [
      render(c),
      i + 1 < l ? operators[formula.type] : undefined
    ]);
  if (formula.type in functions)
    return functions[formula.type](formula.children[0]);
  if (formula.type === GateType.Variable) return formula.var;
}

interface Props {
  formula: Gate;
}

class Output extends Component<Props> {
  render({ formula }: Props) {
    return <div id="output">{render(formula)}</div>;
  }
}

export default Output;
