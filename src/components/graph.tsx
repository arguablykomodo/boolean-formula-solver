import "./graph.scss";
import { h, Component } from "preact";
import { Gate } from "../gate/gate";
import render from "../gate/render";

interface Props {
  formula: Gate;
}

class Graph extends Component<Props> {
  render({ formula }: Props) {
    const { gates, paths, texts, w, h: height } = render(formula, 0, 0);
    return (
      <div className="container">
        <svg xmlns="http://www.w3.org/2000/svg" width={w + 10} height={height}>
          <path id="gates" d={gates} />
          <path id="paths" d={paths + `M ${w} ${height / 2} h 10`} />
          <g id="text">{texts}</g>
        </svg>
      </div>
    );
  }
}

export default Graph;
