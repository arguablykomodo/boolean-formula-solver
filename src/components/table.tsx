import "./table.scss";
import { h, Component } from "preact";
import { Gate, GateType } from "../gate/gate";
import { fromFormula, toFormula } from "../gate/table";

interface Props {
  vars: string[];
  formula: Gate;
  onChange: (formula: Gate) => any;
}

interface State {
  table: boolean[];
  error?: Error;
}

class Table extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { table: fromFormula(props.vars, props.formula) };
  }

  componentWillReceiveProps({ vars, formula }: Props) {
    this.setState({ table: fromFormula(vars, formula), error: undefined });
  }

  modifyTable = (bitChange: number) => {
    this.state.table[bitChange] = !this.state.table[bitChange];
    try {
      this.props.onChange(toFormula(this.state.table, this.props.vars));
    } catch (e) {
      this.setState({ error: e });
    }
  };

  render({ vars }: Props, { table, error }: State) {
    const rows = [];
    const pad = new Array(vars.length).join("0");
    for (let i = 0; i < 2 ** vars.length; i++) {
      const binary = (pad + i.toString(2)).slice(-vars.length);
      rows.push(
        <div key={i} className="row">
          <div className="binary">{binary}</div>
          <div className="computed" onClick={() => this.modifyTable(i)}>
            {table[i] ? "1" : "0"}
          </div>
        </div>
      );
    }
    return (
      <div className="container">
        <div className={error ? "errorMsg" : ""}>
          {error ? error.message : ""}
        </div>
        <div id="table" className={error ? "error" : ""}>
          <div className="header">{vars.join("")}</div>
          {rows}
        </div>
      </div>
    );
  }
}

export default Table;
