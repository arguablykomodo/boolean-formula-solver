import "./input.scss";
import { h, Component } from "preact";
import { Gate } from "../gate/gate";
import equal from "fast-deep-equal";
import toString from "../gate/toString";
import compile from "../gate/compile";

interface Props {
  formula: Gate;
  onChange: (vars: string[], formula: Gate) => any;
}

interface State {
  error?: Error;
  input: string;
  formula: Gate;
}

class Input extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { input: toString(props.formula), formula: props.formula };
  }

  componentWillReceiveProps({ formula }: Props) {
    const diff = !equal(formula, this.state.formula);
    this.setState({
      input: diff ? toString(formula) : this.state.input,
      formula,
      error: undefined
    });
  }

  onChange = (e: Event) => {
    const value = (e.target as HTMLInputElement).value;
    try {
      const { vars, formula } = compile(value);
      this.setState({ error: undefined, input: value, formula }, () =>
        this.props.onChange(vars, formula)
      );
    } catch (e) {
      this.setState({ error: e, input: value });
    }
  };

  render(_, { error, input }: State) {
    return (
      <div id="input" className={error ? "error" : ""}>
        <div>{error ? error.message : ""}</div>
        <input type="text" value={input} onInput={this.onChange} />
      </div>
    );
  }
}

export default Input;
