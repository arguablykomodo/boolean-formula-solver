import "./app.scss";
import { h, Component } from "preact";
import { Gate } from "../gate/gate";
import Input from "./input";
import Output from "./output";
import Table from "./table";
import Graph from "./graph";
import compile from "../gate/compile";

interface State {
  vars: string[];
  formula: Gate;
}

class App extends Component<{}, State> {
  constructor() {
    super();
    const storedState = localStorage.getItem("state");
    if (storedState) this.state = JSON.parse(storedState);
    else this.state = compile("ab+cd");
  }

  onTextChange = (vars: string[], formula: Gate) => {
    this.setState({ vars, formula }, () => {
      localStorage.setItem("state", JSON.stringify(this.state));
    });
  };

  onTableChange = (formula: Gate) => {
    this.setState({ formula }, () => {
      localStorage.setItem("state", JSON.stringify(this.state));
    });
  };

  render(_, { vars, formula }: State) {
    return (
      <div id="app">
        <Input formula={formula} onChange={this.onTextChange} />
        <Output formula={formula} />
        <Table vars={vars} formula={formula} onChange={this.onTableChange} />
        <Graph formula={formula} />
      </div>
    );
  }
}

export default App;
