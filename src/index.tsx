// Magic service worker stuff
if ("serviceWorker" in navigator) {
  const url = "./service-worker.js";
  navigator.serviceWorker.register(url);
}

import { h, render } from "preact";
import App from "./components/app";

const mountPoint = document.getElementById("mount");
render(<App />, mountPoint, mountPoint.lastElementChild);
